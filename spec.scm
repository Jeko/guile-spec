(define-module (spec)
  #:use-module (spec runner)
  #:use-module (spec api)
  #:use-module (spec coverage)
  #:re-export (describe
		  it
		context
		install-spec-runner-term
		install-spec-runner-repl
		test-coverage))

